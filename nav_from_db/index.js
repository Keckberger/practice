
var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;

var lang = "ger"; //default language

//change language if sprache/:lang is triggered
router.get('/sprache/:lang',
  function(req,res,next)  {
    lang = req.params.lang;
    res.redirect('/');
  }
);



/* GET home page. */
router.get('/', function(req, res, next) {
  MongoClient.connect("mongodb://localhost:27017/website_data_from_db", function(err,db)  {
    if(err) {return console.log(err);}


    var collection = db.collection('navigation');
    collection.find({"language":lang}).toArray(function(err, result){


    res.render('index', {title: 'Express', navigation: result[0], lang: lang});
      console.log(result[0].items);
  });


});
});


module.exports = router;
