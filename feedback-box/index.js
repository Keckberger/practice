var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27017/test';

var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false })

var time = new Date().getTime();
var time2 = new Date().getTime();
time2 = time2 + 3600000;
var date2 = new Date(time2);

var date = new Date(time);
console.log(date.toString());
console.log(date2.toString());

//recieve feedback form
router.post('/feedback', urlencodedParser, function(req,res){
    // define what should be inserted into feedback db
    var feedback =  {
    "language": "ger",
    "feedback_value": req.body.feedback_value,
    "username": "Dummy",
    "date": Date()  }; //in der userdatenbank sollte gespeichert werden, wann der user das letzte mal ein Feedback gepostet hat...
                          //... die abfrage ist bestimmt schneller als wenn ich in der feedback db nach user, date und next date suche muss

//insert data into feedback db
MongoClient.connect(url, function(err,db) {
  db.collection('feedback').insert(feedback, function(err, records) {
    if (err) throw err;
    console.log("Document inserted");
  });
});
  res.render('index', {title: 'Express', feedback_submit: 'success', feedback: false});
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express', feedback_submit: false, feedback: false});
});


//show feedback
router.get('/show_feedback/:select', function(req, res)  {
  if(req.params.select == "all")  {
    var language = {};  }
  else {
    var language = {"language":req.params.select};  }

  MongoClient.connect(url, function(err, db)  {
    if(err) throw err;
    var collection = db.collection('feedback');
    collection.find(language).limit(10).sort([["date", 'desc']]).toArray(function(err, result){
      if(err) throw err;
      console.log('feedback Output:');

      res.render('index', {title: 'Express', feedback_submit: false, feedback: result});
    });
  });
});





module.exports = router;
