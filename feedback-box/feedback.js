var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;

var ObjectID = require('mongodb').ObjectID;

var url = 'mongodb://localhost:27017/test';

var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false })

router.get('/', function(req, res, next){
  res.send("feedback page", req.params);
});

router.get('/edit', function(req, res){
  console.log("edit page", req.params);
  res.send('edit page');
});


//load current data from db to html form to edit
router.get('/edit/:id', function(req, res){
  //check here if the user is allowed to load data from db into edit form, if false -> error
  MongoClient.connect(url, function(err, db){
    if(err) throw err;
    var collection = db.collection('feedback');

    feedback_ID = new ObjectID(req.params.id);

    collection.find({_id:feedback_ID}).toArray(function(err, result){
      if(err) throw err;

      res.render('feedback', {id:req.params.id, feedback_data: result});
    });
  });
});

//submit edited data back to db
router.post('/edit_submit', urlencodedParser, function(req, res){
  //check if the user is allowed to update the db
  MongoClient.connect(url, function(err, db){
    if(err) throw err;
    var collection = db.collection('feedback');

    feedback_ID = new ObjectID(req.body.feedback_id); // create ObjectID for mongodb

    collection.update({_id:feedback_ID}, { $set: {"feedback_value": req.body.feedback_value}}, function(err, results)  {
      if(err) throw err;

      res.render('index', {title: 'Express', feedback_submit: 'edited', feedback: false}); //immer soviele variablen händich eintragen? irgendwie doof... wenn sich eines ändert muss alles gändert werden...
    });
  });
});


router.get('/delete/:id', function(req, res){
  //check if the user is allwed to delete
  MongoClient.connect(url, function(err, db){
    if(err) throw err;
    var collection = db.collection('feedback');

    feedback_ID = new ObjectID(req.params.id); //create mongodb ObjectID

    collection.deleteOne({_id:feedback_ID}, function(err, result){
      if(err) throw err;
      res.render('index', {title: 'Express', feedback_submit: 'deleted', feedback: false})
    });


  });

});


module.exports = router;
